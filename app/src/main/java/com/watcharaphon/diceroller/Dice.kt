package com.watcharaphon.diceroller
/**
 * Roll the dice and update the screen with the result.
 */
class Dice(val numSides: Int) {
    fun roll(): Int {
        return (1..numSides).random()
    }
}